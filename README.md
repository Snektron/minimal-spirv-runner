# Minimal SPIR-V Runner

This is a minimal SPIR-V runner to run test executables with OpenCL.
## Usage

```
minimal-spirv-runner [--platform <platform>] [--device <device>] <spirv module> <kernel name>
```

This program loads and launches the entry point named `<kernel name>` from the spir-v module passed in `<spirv module>` with one thread and one workgroup. `--platform` and `--device` can be used to select the platform and device that the kernel will be launched on.

## Compiling

Using `make`. Requires OpenCL libraries and headers.
