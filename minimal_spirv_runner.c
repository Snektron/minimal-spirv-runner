#define CL_TARGET_OPENCL_VERSION 300
#include <CL/cl.h>

#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define MAX_PARAMS 32

const char* clstrerror(cl_int status) {
    switch (status) {
        case CL_SUCCESS: return "CL_SUCCESS";
        case CL_DEVICE_NOT_FOUND: return "CL_DEVICE_NOT_FOUND";
        case CL_DEVICE_NOT_AVAILABLE: return "CL_DEVICE_NOT_AVAILABLE";
        case CL_COMPILER_NOT_AVAILABLE: return "CL_COMPILER_NOT_AVAILABLE";
        case CL_MEM_OBJECT_ALLOCATION_FAILURE: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
        case CL_OUT_OF_RESOURCES: return "CL_OUT_OF_RESOURCES";
        case CL_OUT_OF_HOST_MEMORY: return "CL_OUT_OF_HOST_MEMORY";
        case CL_PROFILING_INFO_NOT_AVAILABLE: return "CL_PROFILING_INFO_NOT_AVAILABLE";
        case CL_MEM_COPY_OVERLAP: return "CL_MEM_COPY_OVERLAP";
        case CL_IMAGE_FORMAT_MISMATCH: return "CL_IMAGE_FORMAT_MISMATCH";
        case CL_IMAGE_FORMAT_NOT_SUPPORTED: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
        case CL_BUILD_PROGRAM_FAILURE: return "CL_BUILD_PROGRAM_FAILURE";
        case CL_MAP_FAILURE: return "CL_MAP_FAILURE";
        case CL_MISALIGNED_SUB_BUFFER_OFFSET: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
        case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
        case CL_COMPILE_PROGRAM_FAILURE: return "CL_COMPILE_PROGRAM_FAILURE";
        case CL_LINKER_NOT_AVAILABLE: return "CL_LINKER_NOT_AVAILABLE";
        case CL_LINK_PROGRAM_FAILURE: return "CL_LINK_PROGRAM_FAILURE";
        case CL_DEVICE_PARTITION_FAILED: return "CL_DEVICE_PARTITION_FAILED";
        case CL_KERNEL_ARG_INFO_NOT_AVAILABLE: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";
        case CL_INVALID_VALUE: return "CL_INVALID_VALUE";
        case CL_INVALID_DEVICE_TYPE: return "CL_INVALID_DEVICE_TYPE";
        case CL_INVALID_PLATFORM: return "CL_INVALID_PLATFORM";
        case CL_INVALID_DEVICE: return "CL_INVALID_DEVICE";
        case CL_INVALID_CONTEXT: return "CL_INVALID_CONTEXT";
        case CL_INVALID_QUEUE_PROPERTIES: return "CL_INVALID_QUEUE_PROPERTIES";
        case CL_INVALID_COMMAND_QUEUE: return "CL_INVALID_COMMAND_QUEUE";
        case CL_INVALID_HOST_PTR: return "CL_INVALID_HOST_PTR";
        case CL_INVALID_MEM_OBJECT: return "CL_INVALID_MEM_OBJECT";
        case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
        case CL_INVALID_IMAGE_SIZE: return "CL_INVALID_IMAGE_SIZE";
        case CL_INVALID_SAMPLER: return "CL_INVALID_SAMPLER";
        case CL_INVALID_BINARY: return "CL_INVALID_BINARY";
        case CL_INVALID_BUILD_OPTIONS: return "CL_INVALID_BUILD_OPTIONS";
        case CL_INVALID_PROGRAM: return "CL_INVALID_PROGRAM";
        case CL_INVALID_PROGRAM_EXECUTABLE: return "CL_INVALID_PROGRAM_EXECUTABLE";
        case CL_INVALID_KERNEL_NAME: return "CL_INVALID_KERNEL_NAME";
        case CL_INVALID_KERNEL_DEFINITION: return "CL_INVALID_KERNEL_DEFINITION";
        case CL_INVALID_KERNEL: return "CL_INVALID_KERNEL";
        case CL_INVALID_ARG_INDEX: return "CL_INVALID_ARG_INDEX";
        case CL_INVALID_ARG_VALUE: return "CL_INVALID_ARG_VALUE";
        case CL_INVALID_ARG_SIZE: return "CL_INVALID_ARG_SIZE";
        case CL_INVALID_KERNEL_ARGS: return "CL_INVALID_KERNEL_ARGS";
        case CL_INVALID_WORK_DIMENSION: return "CL_INVALID_WORK_DIMENSION";
        case CL_INVALID_WORK_GROUP_SIZE: return "CL_INVALID_WORK_GROUP_SIZE";
        case CL_INVALID_WORK_ITEM_SIZE: return "CL_INVALID_WORK_ITEM_SIZE";
        case CL_INVALID_GLOBAL_OFFSET: return "CL_INVALID_GLOBAL_OFFSET";
        case CL_INVALID_EVENT_WAIT_LIST: return "CL_INVALID_EVENT_WAIT_LIST";
        case CL_INVALID_EVENT: return "CL_INVALID_EVENT";
        case CL_INVALID_OPERATION: return "CL_INVALID_OPERATION";
        case CL_INVALID_GL_OBJECT: return "CL_INVALID_GL_OBJECT";
        case CL_INVALID_BUFFER_SIZE: return "CL_INVALID_BUFFER_SIZE";
        case CL_INVALID_MIP_LEVEL: return "CL_INVALID_MIP_LEVEL";
        case CL_INVALID_GLOBAL_WORK_SIZE: return "CL_INVALID_GLOBAL_WORK_SIZE";
        case CL_INVALID_PROPERTY: return "CL_INVALID_PROPERTY";
        case CL_INVALID_IMAGE_DESCRIPTOR: return "CL_INVALID_IMAGE_DESCRIPTOR";
        case CL_INVALID_COMPILER_OPTIONS: return "CL_INVALID_COMPILER_OPTIONS";
        case CL_INVALID_LINKER_OPTIONS: return "CL_INVALID_LINKER_OPTIONS";
        case CL_INVALID_DEVICE_PARTITION_COUNT: return "CL_INVALID_DEVICE_PARTITION_COUNT";
        case CL_INVALID_PIPE_SIZE: return "CL_INVALID_PIPE_SIZE";
        case CL_INVALID_DEVICE_QUEUE: return "CL_INVALID_DEVICE_QUEUE";
        case CL_INVALID_SPEC_ID: return "CL_INVALID_SPEC_ID";
        case CL_MAX_SIZE_RESTRICTION_EXCEEDED: return "CL_MAX_SIZE_RESTRICTION_EXCEEDED";
        default: return "(unknown error)";
    }
}

#define CHECK_CL(x) \
    { \
        cl_int _status = (x); \
        if (_status != CL_SUCCESS) { \
            const char* _msg = clstrerror(_status); \
            fprintf(stderr, __FILE__ ":%d: error: opencl returned %s (%d)\n", __LINE__, _msg, _status); \
            exit(EXIT_FAILURE); \
        } \
    }

char* platform_get_name(cl_platform_id platform) {
    size_t size;
    CHECK_CL(clGetPlatformInfo(platform, CL_PLATFORM_NAME, 0, NULL, &size));
    char* name = malloc(size);
    CHECK_CL(clGetPlatformInfo(platform, CL_PLATFORM_NAME, size, name, NULL));
    return name;
}

char* device_get_name(cl_device_id device) {
    size_t size;
    CHECK_CL(clGetDeviceInfo(device, CL_DEVICE_NAME, 0, NULL, &size));
    char* name = malloc(size);
    CHECK_CL(clGetDeviceInfo(device, CL_DEVICE_NAME, size, name, NULL));
    return name;
}

bool device_supports_spirv(cl_device_id device) {
    size_t il_bytes;
    CHECK_CL(clGetDeviceInfo(device, CL_DEVICE_ILS_WITH_VERSION, 0, NULL, &il_bytes));
    cl_name_version* versions = malloc(il_bytes);
    CHECK_CL(clGetDeviceInfo(device, CL_DEVICE_ILS_WITH_VERSION, il_bytes, versions, NULL));

    for (size_t i = 0; i < il_bytes / sizeof(cl_name_version); ++i) {
        if (strstr(versions[i].name, "SPIR-V") != 0) {
            return true;
        }
    }

    return false;
}

int main(int argc, char* argv[]) {
    char* program_path = NULL;
    char* kernel_name = NULL;
    char* platform_query = NULL;
    char* device_query = NULL;

    int params = 0;
    long long param_sizes[MAX_PARAMS] = {-1};

    for (int i = 1; i < argc; ++i) {
        char* arg = argv[i];
        if (strcmp(arg, "--platform") == 0) {
            if (++i == argc) {
                fprintf(stderr, "error: --platform expects argument <platform>\n");
                return EXIT_FAILURE;
            }
            platform_query = argv[i];
        } else if (strcmp(arg, "--device") == 0) {
            if (++i == argc) {
                fprintf(stderr, "error: --device expects argument <device>\n");
                return EXIT_FAILURE;
            }
            device_query = argv[i];
        } else if (strcmp(arg, "--param") == 0) {
            if (++i == argc) {
                fprintf(stderr, "error: --param expects argument <size>\n");
                return EXIT_FAILURE;
            }

            if (++params == MAX_PARAMS) {
                fprintf(stderr, "error: too many parameters\n");
                return EXIT_FAILURE;
            }

            if (sscanf(argv[i], "%lld", &param_sizes[params - 1]) != 1 || param_sizes[params - 1] <= 0) {
                fprintf(stderr, "error: invalid --param argument '%s'\n", argv[i]);
                return EXIT_FAILURE;
            }
        } else if (!program_path) {
            program_path = arg;
        } else if (!kernel_name) {
            kernel_name = arg;
        } else {
            fprintf(stderr, "unknown argument: %s\n", arg);
            return EXIT_FAILURE;
        }
    }

    if (!program_path || !kernel_name) {
        fputs(
            "usage: minimal-spirv-runner [options] <file> <kernel name>\n"
            "\n"
            "options:\n"
            "--platform <platform>  The OpenCL platform to use\n"
            "--device <device>      The OpenCL device to use\n"
            "--param <size>         Pass a buffer of <size> bytes to the kernel. May be\n"
            "                       specified up to 32 times.\n"
            "<file>                 The OpenCL program to compile and run. This may be\n"
            "                       either a SPIR-V program (file extension: .spv) or an\n"
            "                       OpenCL C program (file extension: .spv)\n",
            stderr
        );
        return EXIT_FAILURE;
    }

    const char* ext = strrchr(program_path, '.');
    bool spirv;
    if (strcmp(ext, ".spv") == 0) {
        spirv = true;
    } else if (strcmp(ext, ".cl") == 0) {
        spirv = false;
    } else {
        fprintf(stderr, "error: couldn't determine file type of program '%s'", program_path);
        fprintf(stderr, "please name the file '%s.spv' or '%s.cl'", program_path, program_path);
        return EXIT_FAILURE;
    }

    int fd = open(program_path, O_RDONLY);
    if (!fd) {
        fprintf(stderr, "failed to load module\n");
        return EXIT_FAILURE;
    }
    off_t size = lseek(fd, 0, SEEK_END);
    lseek(fd, 0, SEEK_SET);

    char* program_data = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
    close(fd);

    if (program_data == MAP_FAILED) {
        fprintf(stderr, "failed to load OpenCL program %s\n", program_path);
    }

    cl_uint num_platforms;
    CHECK_CL(clGetPlatformIDs(0, NULL, &num_platforms));
    cl_platform_id* platforms = malloc(num_platforms * sizeof(cl_platform_id));
    CHECK_CL(clGetPlatformIDs(num_platforms, platforms, NULL));

    cl_platform_id platform;
    cl_device_id device;
    for (cl_uint i = 0; i < num_platforms; ++i) {
        char* platform_name = platform_get_name(platforms[i]);
        if (platform_query && strstr(platform_name, platform_query) == 0) {
            free(platform_name);
            continue;
        }

        cl_uint num_devices;
        cl_int status = clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, 0, NULL, &num_devices);
        if (status == CL_DEVICE_NOT_FOUND) {
            free(platform_name);
            continue;
        }
        CHECK_CL(status);
        cl_device_id* devices = malloc(num_devices * sizeof(cl_device_id));
        CHECK_CL(clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, num_devices, devices, NULL));

        for (cl_uint j = 0; j < num_devices; ++j) {
            char* device_name = device_get_name(devices[j]);
            if (device_query && strstr(device_name, device_query) == 0) {
                free(device_name);
                continue;
            }

            if (spirv && !device_supports_spirv(devices[j])) {
                free(device_name);
                continue;
            }

            printf("selected platform '%s' and device '%s'\n", platform_name, device_name);

            platform = platforms[i];
            device = devices[j];

            free(platforms);
            free(devices);
            free(device_name);
            goto device_selected;
        }

        free(devices);
        free(platform_name);
    }
    free(platforms);

    fprintf(stderr, "failed to select device\n");
    return EXIT_FAILURE;

device_selected:
    cl_context_properties props[] = {CL_CONTEXT_PLATFORM, (cl_context_properties) platform, 0};
    cl_int status;
    cl_context context = clCreateContext(props, 1, &device, NULL, NULL, &status);
    CHECK_CL(status);

    cl_command_queue queue = clCreateCommandQueueWithProperties(context, device, NULL, &status);
    CHECK_CL(status);

    cl_program program;
    if (spirv) {
        program = clCreateProgramWithIL(
            context,
            (uint32_t*)program_data,
            size,
            &status
        );
    } else {
        const char* source = program_data;
        program = clCreateProgramWithSource(
            context,
            1,
            &source,
            &size,
            &status
        );
    }
    CHECK_CL(status);

    status = clBuildProgram(program, 1, &device, NULL, NULL, NULL);
    if (status == CL_BUILD_PROGRAM_FAILURE) {
        size_t log_size;
        CHECK_CL(clGetProgramBuildInfo(
            program,
            device,
            CL_PROGRAM_BUILD_LOG,
            0,
            NULL,
            &log_size
        ));
        char* log = malloc(log_size);
        CHECK_CL(clGetProgramBuildInfo(
            program,
            device,
            CL_PROGRAM_BUILD_LOG,
            log_size,
            log,
            NULL
        ));

        fprintf(stderr, "error: failed to compile kernel:\n");
        fputs(log, stderr);

        return EXIT_FAILURE;
    }
    CHECK_CL(status);

    cl_kernel kernel = clCreateKernel(program, kernel_name, &status);
    CHECK_CL(status);

    for (int i = 0; i < MAX_PARAMS; ++i) {
        const long long param_size = param_sizes[i];
        if (param_size <= 0) {
            break;
        }

        cl_mem param = clCreateBuffer(context, CL_MEM_READ_WRITE, param_size, NULL, &status);
        CHECK_CL(status);
        CHECK_CL(clSetKernelArg(kernel, i, sizeof(param), &param));
    }

    cl_event kernel_done;
    size_t global_wrk = 1;
    size_t local_wrk = 1;
    CHECK_CL(clEnqueueNDRangeKernel(
        queue,
        kernel,
        1,
        NULL,
        &global_wrk,
        &local_wrk,
        0,
        NULL,
        &kernel_done
    ));

    CHECK_CL(clWaitForEvents(1, &kernel_done));

    clReleaseProgram(program);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
    munmap(program_data, size);
}
